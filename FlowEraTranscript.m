classdef FlowEraTranscript < EraTranscript
    %FlowEraTranscript Transcript of an era with a continuous flow.
    %   The states in our case are visited for time intervals of the form
    %   [t_s, t_e). In the duration of the time interval the state is
    %   constant. Note that t_e >= t_s.
    
    % Transcript/FlowEraTranscript.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        % Continuous trajectory data
        Ts % Continuous transition time interval
        ZXs % Continuous transition trajectory
        ZYs % Memoized continuous output
        % Data required to for the map invertion (hologram)
        x_init
        
        % Events during continuous transition
        te
        zxe
        ie
        
        % Projection onto the flow space: [x] = hologram(hp, x_init, zx)
        holographicProjection % == hp
    end
    
    methods
        function [fet] = FlowEraTranscript(discreteStateFlow, flowTranscript)
            if isempty(flowTranscript)
                msgID = 'SolverError:EmptyFlowTranscript';
                msgtext = 'Error occur in the integrator. Solution was not continued.';
                ME = MException(msgID, msgtext);
                throw(ME);
            end
            
            Ts = flowTranscript.storedTime();
            if length(Ts) < 1 % Continuous transition
                msgID = 'SolverError:SingleElementOutput';
                msgtext = 'Error occur in the integrator. Solution was not continued and contains single element.';
                ME = MException(msgID, msgtext);
                throw(ME);
            end
            ZXs = flowTranscript.storedTrajectory();
            x_init = flowTranscript.initialState();
            ZYs = outputTimeSerries(Ts, x_init, ZXs);
            
            holographicProjection = discreteStateFlow.getHolographicProjection();
            
            % Terminal event and points
            t_fin = Ts(end);
            zx_fin = ZXs(:,end);
            x_fin = holographicProjection.hologram(x_init, zx_fin);
            e_term = flowTranscript.e_idx();
            
            fet@EraTranscript(discreteStateFlow, ...
                t_fin, x_fin, e_term);
            
            % Trajectory data
            fet.Ts = Ts;
            fet.ZXs = ZXs;
            fet.ZYs = ZYs;
            % Holographic inversion data
            fet.x_init = x_init;
            fet.holographicProjection = holographicProjection;
            
            % Events during continuous transition
            [te, zxe, ie] = flowTranscript.eventsProj();
            fet.te = te;
            fet.zxe = zxe;
            fet.ie = ie;
            
            function [zy] = outputTimeSerries(t, x_init, zx)
                % Evaluate the output function using the holographic
                % projection to compute the full state required for
                % the evaluation of the output.
                holographicProjection = discreteStateFlow.getHolographicProjection();
                dimensions = discreteStateFlow.compressedContinuumOutputSize();
                
                N_time_instances = length(t);
                zy = zeros(dimensions, N_time_instances);
                for n = 1:N_time_instances
                    t_n = t(n);
                    zx_n = zx(:,n);
                    x_n = holographicProjection.hologram(x_init, zx_n);
                    y_n = discreteStateFlow.continuumOutput(t_n, x_n);
                    zy(:,n) = discreteStateFlow.compressContinuumOutput(y_n);
                end
            end
        end
        
        function [t] = timeInstances(fet)
            t = fet.Ts;
        end
        
        function [x] = trajectory(fet)
            dimensions = length(fet.x_init);
            N = length(fet.Ts);
            x = zeros(dimensions, N);
            for n = 1:N
                zx_n = fet.ZXs(:,n);
                x_n = fet.holographicProjection.hologram(fet.x_init, zx_n);
                x(:,n) = x_n;
            end
        end
        
        function [x, y] = continuumSignals(fet)
            dimensions_x = length(fet.x_init);
            dimensions_y = fet.continuumOutputSize;
            N = length(fet.Ts);
            x = zeros(dimensions_x, N);
            y = zeros(dimensions_y, N);
            for n = 1:N
                zx_n = fet.ZXs(:,n);
                x_n = fet.holographicProjection.hologram(fet.x_init, zx_n);
                x(:,n) = x_n;
                zy_n = fet.ZYs(:,n);
                y_n = fet.outputHandler.reconstructContinuumOutput(t_n, x_n, zy_n);
                y(:,n) = y_n;
            end
        end
        
        function [y] = continuumOutputSignal(fet)
            dimensions_y = fet.continuumOutputSize;
            N = length(fet.Ts);
            y = zeros(dimensions_y, N);
            for n = 1:N
                zx_n = fet.ZXs(:,n);
                x_n = fet.holographicProjection.hologram(fet.x_init, zx_n);
                zy_n = fet.ZYs(:,n);
                t_n = fet.Ts(n);
                y_n = fet.outputHandler.reconstructContinuumOutput(t_n, x_n, zy_n);
                y(:,n) = y_n;
            end
        end
        
        function [u] = discreteOutputSignal(fet)
            u = fet.discreteOutput() .* ones(size(fet.Ts));
        end
        
        function [n] = size(fet)
            n = length(fet.Ts);
        end
        
        function [t, x, e] = eraEvents(fet)
            t = jet.te;
            e = jet.ie;
            
            dimensions_x = length(fet.x_init);
            N = length(fet.Ts);
            x = zeros(dimensions_x, N);
            for n = 1:N
                zxe_n = fet.zxe(:,n);
                x(:,n) = fet.holographicProjection.hologram(fet.x_init, zxe_n);
            end
        end
    end
    
end

