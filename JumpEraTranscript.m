classdef JumpEraTranscript < EraTranscript
    %JumpEraTranscript Transcript of an era with a single jump.
    %   Detailed explanation goes here
    
    % Transcript/JumpEraTranscript.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
    end
    
    methods
        function [jet] = JumpEraTranscript(discreteStateJump, t, x, e)
            jet@EraTranscript(discreteStateJump, t, x, e);
        end
        
        function [t] = timeInstances(jet)
            t = jet.t;
        end
        
        function [x] = trajectory(jet)
            x = jet.x;
        end
        
        function [x, y] = continuumSignals(jet)
            x = jet.x;
            y = jet.finalContinuumOutput();
        end
        
        function [y] = continuumOutputSignal(jet)
            y = jet.finalContinuumOutput();
        end
        
        function [u] = discreteOutputSignal(jet)
            u = jet.discreteOutput();
        end
        
        function [n] = size(~)
            n = 1;
        end
        
        function [t, x, e] = eraEvents(jet)
            t = jet.t;
            x = jet.x;
            e = jet.e;
        end
    end
    
end

