classdef (Abstract) EraTranscript
    %EraTranscript Transcript of a single era; each era is either a single
    %jump, of a continuous flow
    %   This abstract class defines a unified interface for the transcipt
    %   of a single era.
    
    % Transcript/EraTranscript.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        discreteStateEnumeration % The discrete state of the current era
        u % discrete state output
        
        t % Final time instance of the era
        x % Final continuous state of the era
        e % Event that ends the discrete state era
        
        zy % Final continuum output (compressed)
        
        % Output memoization functions
        outputHandler
        continuumOutputSize
        %[y] = reconstructContinuumOutput(ds, t, x, zy);
        
        %hologram
        %map
        %compressedContinuumOutputSize
        %continuumOutput
        %compressContinuumOutput
        %reconstructContinuumOutput
    end
    
    methods (Abstract)
        [t] = timeInstances(et);
        [x] = trajectory(et);
        [x, y] = continuumSignals(et);
        [y] = continuumOutputSignal(et);
        [u] = discreteOutputSignal(et);
        [n] = size(et);
        [te, xe, ie] = eraEvents(et);
        % For composable automata (not-implemented)
        % [et] = merge(et, x, map);
    end
    
    methods
        function [et] = EraTranscript(discreteState, ...
                t, x, e)
            et.discreteStateEnumeration = discreteState.discreteStateEnumeration;
            et.u = discreteState.discreteOutput();
            
            et.outputHandler = discreteState.getOutputHandler();
            
            et.t = t;
            et.x = x;
            et.e = e;
            
            y = discreteState.continuumOutput(t, x);
            et.zy = discreteState.compressContinuumOutput(y);
            
            et.continuumOutputSize = discreteState.continuumOutputSize();
        end
               
        function [t, x] = finalContinuumState(et)
            % Get the final state and time instance.
            t = et.t;
            x = et.x;
        end
        
        function [y] = finalContinuumOutput(et)
            % Get the continuum output in the final time instance of the
            % trnascript.
            y = et.outputHandler.reconstructContinuumOutput(et.t, et.x, et.zy);
        end
        
        function [u] = discreteOutput(et)
            % Get the discrete output of the state. The discrete output is
            % a time invariant for the transcript of a flow era.
            u = et.u;
        end
        
        function [discreteStateEnumeration] = eraState(et)
            % Get the discrete state of the current era.
            discreteStateEnumeration = et.discreteStateEnumeration;
            %discreteState = discreteStateGenerator.generateState(et.stateId);
        end
    end
    
end

