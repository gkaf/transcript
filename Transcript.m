classdef Transcript < DiscreteStateData
    %Transcript Data of the discrete state for hybrid automata avaluating
    %the automaton transcript.
    %   The class stores of the transcript of the evaluation of a hybrid
    %   automaton. This a simplest and frequenctly used operation.
    
    % Transcript/Transcript.m
    % Author: Georgios Kafanas
    % Institute: University of Bristol
    % Year: 2018
    % Contact: georgios.kafanas@bristol.ac.uk
    
    properties
        discreteStateGenerator % To construct any discrete state from the transcript elements it generated
        doubleEndedQueue_EraTranscript % A double ended que of all the trascript elements in the cronological order the where generated
    end
    
    methods (Abstract)
        %[finalizedTranscript] = finalize(ht);
    end
    
    methods
        function [ht] = Transcript(discreteStateGenerator)
            ht@DiscreteStateData();
            ht.discreteStateGenerator = discreteStateGenerator;
            ht.doubleEndedQueue_EraTranscript = DoubleEndedQueue();
        end
        
        function [n] = eras(ht)
            % Outputs the number of the function eras.
            deq = ht.doubleEndedQueue_EraTranscript;
            n = deq.size();
        end
        
        function [eras] = getEras(ht)
            % Outputs an array of the function eras.
            deq = ht.doubleEndedQueue_EraTranscript;
            eras = deq.toCell();
        end
        
        function [ind] = isEmpty(ht)
            deq = ht.doubleEndedQueue_EraTranscript;
            ind = deq.isEmpty();
        end
        
        function [eraTranscript] = getEraTranscript(ht)
            deq = ht.doubleEndedQueue_EraTranscript;
            eraTranscript = deq.back();
        end
        
        function [ht] = previousEra(ht)
            deq = ht.doubleEndedQueue_EraTranscript;
            [deq, ~] = deq.removeBack();
            ht.doubleEndedQueue_EraTranscript = deq;
        end
        
        function [ht] = extend(ht, eraTranscript)
            deq = ht.doubleEndedQueue_EraTranscript;
            deq = deq.addBack(eraTranscript);
            ht.doubleEndedQueue_EraTranscript = deq;
        end
        
        function [ht] = append(ht_prefix, ht_postfix)
            deq_prefix = ht_postfix.doubleEndedQueue_EraTranscript;
            deq_postfix = ht_postfix.doubleEndedQueue_EraTranscript;
            if ht_prefix.discreteStateGenerator.eq(ht_postfix.discreteStateGenerator)
                deq = deq_prefix.preappend(deq_postfix);
                ht = Transcript(ht_prefix.discreteStateGenerator);
                ht.doubleEndedQueue_EraTranscript = deq;
            else
                ht = {};
            end
        end
    end
    
    methods (Access = private)
        function [time, signals_1, signals_2] = getSignals(ht, fcn_1, fcn_2)
            N_1 = length(fcn_1);
            signals_1_stack = cell(N_1,1);
            for n = 1:N_1
                signals_1_stack{n} = Stack();
            end
            
            N_2 = length(fcn_2);
            signals_2_stack = cell(N_2,2);
            for n = 1:N_2
                signals_2_stack{n,1} = Stack();
                signals_2_stack{n,2} = Stack();
            end
            
            time_stack = Stack();
            
            iter = ht.doubleEndedQueue_EraTranscript.getIterator();
            while iter.hasHead()
                eraTranscript = iter.getHead();
                
                t = eraTranscript.timeInstances();
                time_stack = time_stack.push(t);
                for n = 1:N_1
                    f = fcn_1{n};
                    y = f(eraTranscript);
                    signals_1_stack{n} = signals_1_stack{n}.push(y);
                end
                for n = 1:N_2
                    f = fcn_2{n};
                    [x,y] = f(eraTranscript);
                    signals_2_stack{n,1} = signals_2_stack{n,1}.push(x);
                    signals_2_stack{n,2} = signals_2_stack{n,2}.push(y);
                end
                
                iter = iter.nextHead();
            end
            
            time = time_stack.toCell();
            signals_1 = cell(N_1,1);
            for n = 1:N_1
                signals_1{n} = signals_1_stack{n}.toCell();
            end
            signals_2 = cell(N_2,2);
            for n = 1:N_2
                signals_2{n,1} = signals_2_stack{n,1}.toCell();
                signals_2{n,2} = signals_2_stack{n,2}.toCell();
            end
        end
    end
    
    methods
        % All functions containing the state trajectory output a cell
        % containning the transcripts of each era. The continuous state
        % does not have to be on the same vector space for every era.
        function [t, x] = get_tx(ht)
            fcn = {@get_trajectory};
            [time, signals] = ht.getSignals(fcn, {});
            t = time;
            x = signals{1};
            
            function [x] = get_trajectory(eraTranscript)
                x = eraTranscript.trajectory();
            end
        end
        
        function [t, y] = get_ty(ht)
            fcn = {@get_continuumOutputSignal};
            [time, signals] = ht.getSignals(fcn, {});
            t = cell2mat(time);
            y = cell2mat(signals{1});
            
            function [y] = get_continuumOutputSignal(eraTranscript)
                y = eraTranscript.continuumOutputSignal();
            end
        end
        
        function [t, y, u] = get_tyu(ht)
            fcn = {@get_continuumOutputSignal, @get_discreteOutputSignal};
            [time, signals] = ht.getSignals(fcn, {});
            t = cell2mat(time);
            y = cell2mat(signals{1});
            u = cell2mat(signals{2});
            
            function [y] = get_continuumOutputSignal(eraTranscript)
                y = eraTranscript.continuumOutputSignal();
            end
            
            function [u] = get_discreteOutputSignal(eraTranscript)
                u = eraTranscript.discreteOutputSignal();
            end
        end
        
        function [t, x, u] = get_txu(ht)
            fcn = {@get_trajectory, @get_discreteOutputSignal};
            [time, signals] = ht.getSignals(fcn, {});
            t = time;
            x = signals{1};
            u = signals{2};
            
            function [x] = get_trajectory(eraTranscript)
                x = eraTranscript.trajectory();
            end
            
            function [u] = get_discreteOutputSignal(eraTranscript)
                u = eraTranscript.discreteOutputSignal();
            end
        end
        
        function [t, x, y] = get_txy(ht)
            fcn_2 = {@get_continuumSignals};
            [time, ~, signals_2] = ht.getSignals({}, fcn_2);
            t = time;
            x = signals_2{1,1};
            y = signals_2{1,2};
            
            function [x, y] = get_continuumSignals(eraTranscript)
                [x,y] = eraTranscript.continuumSignals();
            end
        end
        
        function [t, x, y, u] = get_txyu(ht)
            fcn_1 = {@get_discreteOutputSignal};
            fcn_2 = {@get_continuumSignals};
            [time, signals_1, signals_2] = ht.getSignals(fcn_1, fcn_2);
            t = time;
            x = signals_2{1,1};
            y = signals_2{1,2};
            u = signals_1{1};
            
            function [u] = get_discreteOutputSignal(eraTranscript)
                u = eraTranscript.discreteOutputSignal();
            end
            
            function [x, y] = get_continuumSignals(eraTranscript)
                [x,y] = eraTranscript.continuumSignals();
            end
        end
    end
    
    
end

